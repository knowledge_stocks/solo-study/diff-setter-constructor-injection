package com.dobda.apiserver;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiserverApplication implements CommandLineRunner {

//    @Autowired private ABeanSetter aBeanSetter;
//    @Autowired private BBeanSetter bBeanSetter;
//
    @Override
    public void run(String... args) throws Exception {
//        aBeanSetter.hello();
//        bBeanSetter.hello();
    }

    public static void main(String[] args) {
		SpringApplication.run(ApiserverApplication.class, args);
	}

}
