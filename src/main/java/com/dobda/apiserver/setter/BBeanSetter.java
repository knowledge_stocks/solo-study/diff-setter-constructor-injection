package com.dobda.apiserver.setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BBeanSetter {

    @Autowired
    private ABeanSetter aBeanSetter;

    public void setaBeanSetter(ABeanSetter aBeanSetter) {
        this.aBeanSetter = aBeanSetter;
    }

    public void hello() {
        aBeanSetter.hello();
    }
}
