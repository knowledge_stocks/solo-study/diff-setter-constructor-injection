package com.dobda.apiserver.setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ABeanSetter {

    @Autowired
    private BBeanSetter bBeanSetter;

    public void setbBeanSetter(BBeanSetter bBeanSetter) {
        this.bBeanSetter = bBeanSetter;
    }

    public void hello() {
        bBeanSetter.hello();
    }
}
